import 'package:flutter/material.dart';
import 'package:popcrn_demo/constants/constants.dart';
import 'package:popcrn_demo/generated_resources/app_localizations.dart';
import 'package:popcrn_demo/navigation/navigator.dart';
import 'package:popcrn_demo/stores/cashout_settings_main_page_store/cashout_settings_main_page_store.dart';
import 'package:popcrn_demo/stores/cashout_settings_page_store/cashout_settings_page_store.dart';
import 'package:popcrn_demo/stores/cashout_settings_store/cashout_settings_store.dart';
import 'package:popcrn_demo/stores/main_page_store/main_page_store.dart';
import 'package:provider/provider.dart';

class PopcrnDemoApp extends StatelessWidget {
  const PopcrnDemoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<CashoutSettings>(create: (_) => CashoutSettings()),
        Provider<MainPageStore>(create: (_) => MainPageStore()),
        Provider<CashoutSettingsPageStore>(
            create: (_) => CashoutSettingsPageStore()),
        Provider<CashoutSettingsMainPageStore>(
            create: (_) => CashoutSettingsMainPageStore()),
      ],
      child: MaterialApp(
        title: 'Popcrn Demo. By YKapustin',
        theme: ConstantsTheme.primaryTheme,
        localizationsDelegates: const [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: AppLocalizations.supportedLocales,
        onGenerateRoute: NavigatorManager.onGenerateRoute,
        initialRoute: NavigatorManager.homePage,
      ),
    );
  }
}
