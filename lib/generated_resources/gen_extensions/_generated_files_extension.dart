const assetsPrefix = "assets/";

class Images {
  String get dSStore => "${assetsPrefix}images/.DS_Store";

  Svgs get svgs => Svgs();

  String get cashoutScreenImagePng =>
      "${assetsPrefix}images/cashout_screen_image.png";

  String get templateProfileImagePng =>
      "${assetsPrefix}images/template_profile_image.png";

  PixelPerfect get pixelPerfect => PixelPerfect();

  String get chocolateGirlPng => "${assetsPrefix}images/chocolate_girl.png";
}

class Svgs {
  String get likeButtonSvg => "${assetsPrefix}images/svgs/like_button.svg";

  String get arrowDownSvg => "${assetsPrefix}images/svgs/arrow_down.svg";

  String get templateProfileImageSvg =>
      "${assetsPrefix}images/svgs/template_profile_image.svg";

  String get cashoutScreenImageSvg =>
      "${assetsPrefix}images/svgs/cashout_screen_image.svg";
}

class PixelPerfect {
  String get welcomeScreenPng =>
      "${assetsPrefix}images/pixel_perfect/welcome_screen.png";

  String get cashoutSettingsScreen1Png =>
      "${assetsPrefix}images/pixel_perfect/cashout_settings_screen_1.png";

  String get cashoutSettingsScreen2Png =>
      "${assetsPrefix}images/pixel_perfect/cashout_settings_screen_2.png";
}

class Languages {
  String get enUSJson => "${assetsPrefix}languages/en_US.json";
}
