/*Внимание! В данном файле нельзя производить изменения вручную. Т.к. генератор просто удалит их.
              Используйте плагин flutter_serve для внесения изменений: flutter pub run flutter_serve*/

class Pages {
  MainPage get mainPage => MainPage();
  CashoutSettingsPages get cashoutSettingsPages => CashoutSettingsPages();
}

class MainPage {
  Content get content => Content();
  Chips get chips => Chips();
  Buttons get buttons => Buttons();
}

class Content {
  ///The Best Color Blast
  ///Collection ‘20.
  String get title =>
      "pages.main_page.content.title"; /*The Best Color Blast 
Collection ‘20.*/
  ///Solomon Mabry
  String get person => "pages.main_page.content.person"; /*Solomon Mabry*/
}

class Chips {
  ///Settings
  String get settingsChip => "pages.main_page.chips.settings_chip"; /*Settings*/
}

class Buttons {
  ///Swipe up
  String get swipeUpButton =>
      "pages.main_page.buttons.swipe_up_button"; /*Swipe up*/
}

class CashoutSettingsPages {
  ///Cashout settings
  String get title => "pages.cashout_settings_pages.title"; /*Cashout settings*/
  SelectCountryPage get selectCountryPage => SelectCountryPage();
  CardPage get cardPage => CardPage();
}

class SelectCountryPage {
  ///Hey Allan! Select a country where your bank account is located.
  String get plsSelectCountryMessage =>
      "pages.cashout_settings_pages.select_country_page.pls_select_country_message"; /*Hey Allan! Select a country where your bank account is located.*/
  ///Connect
  String get connectButton =>
      "pages.cashout_settings_pages.select_country_page.connect_button"; /*Connect*/
  SelectCountryDialog get selectCountryDialog => SelectCountryDialog();
}

class SelectCountryDialog {
  ///Pls, select country
  String get title =>
      "pages.cashout_settings_pages.select_country_page.select_country_dialog.title"; /*Pls, select country*/
  ///Sorry, but right now available only United States
  String get message =>
      "pages.cashout_settings_pages.select_country_page.select_country_dialog.message"; /*Sorry, but right now available only United States*/
  Actions get actions => Actions();
}

class Actions {
  ///United States
  String get usaChooseAction =>
      "pages.cashout_settings_pages.select_country_page.select_country_dialog.actions.usa_choose_action"; /*United States*/
  ///Clear
  String get clearAction =>
      "pages.cashout_settings_pages.select_country_page.select_country_dialog.actions.clear_action"; /*Clear*/
  ///Cancel
  String get cancelAction =>
      "pages.cashout_settings_pages.select_country_page.select_country_dialog.actions.cancel_action"; /*Cancel*/
}

class CardPage {
  ///Hey Allan! You will receive payouts through the bank account or card you have on file.
  String get payoutsInfo =>
      "pages.cashout_settings_pages.card_page.payouts_info"; /*Hey Allan! You will receive payouts through the bank account or card you have on file.*/
  ///Visa
  ///****7309 USD
  String get cardTemplateInfo =>
      "pages.cashout_settings_pages.card_page.card_template_info"; /*Visa
****7309 USD*/
  ///Country:
  String get country =>
      "pages.cashout_settings_pages.card_page.country"; /*Country: */
  ///Change account country
  String get changeCountryButton =>
      "pages.cashout_settings_pages.card_page.change_country_button"; /*Change account country*/
  ///Manage account
  String get manageAccountButton =>
      "pages.cashout_settings_pages.card_page.manage_account_button"; /*Manage account*/
  ///Save & exit
  String get saveExitButton =>
      "pages.cashout_settings_pages.card_page.save_exit_button"; /*Save & exit*/
}
