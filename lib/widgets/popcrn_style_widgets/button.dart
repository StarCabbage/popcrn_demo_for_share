import 'package:flutter/material.dart';
import 'package:popcrn_demo/constants/constants.dart';
import 'package:popcrn_demo/constants/theme_constants/button_constants.dart';

class Button extends StatelessWidget {
  final String textButton;
  final Color? color;
  final GestureTapCallback? onTap;

  const Button(
      {Key? key,
      required this.textButton,
      this.onTap,
      this.color = ConstantsColors.buttonOrangeColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        width: ConstantsThemeButton.buttonWidth,
        height: ConstantsThemeButton.buttonHeight,
        decoration: BoxDecoration(
          borderRadius: ConstantsThemeButton.buttonBorderRadius,
          color: color,
        ),
        child: Text(
          textButton,
          style: ConstantsThemeButton.buttonTextStyle,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
