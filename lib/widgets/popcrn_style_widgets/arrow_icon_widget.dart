import 'package:flutter_svg/flutter_svg.dart';
import 'package:popcrn_demo/constants/constants.dart';
import 'package:popcrn_demo/generated_resources/gen_extensions/generated_files_extension.dart';

class ArrowIcon extends StatelessWidget {
  const ArrowIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      files.images.svgs.arrowDownSvg,
      width: 9.12,
      height: 15.99,
    );
  }
}
