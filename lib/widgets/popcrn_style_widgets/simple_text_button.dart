import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:popcrn_demo/constants/constants.dart';

class SimpleTextButton extends StatelessWidget {
  final String text;
  final GestureTapCallback? onPressed;
  final Color textColor;

  const SimpleTextButton(
      {Key? key,
      required this.text,
      this.onPressed,
      this.textColor = Colors.white})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _style = ConstantsSimpleTextButton.simpleTextButtonTextStyle(textColor);
    return GestureDetector(onTap: onPressed, child: Text(text, style: _style));
  }
}
