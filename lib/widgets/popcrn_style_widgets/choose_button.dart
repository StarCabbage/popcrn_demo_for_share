import 'dart:math';

import 'package:popcrn_demo/constants/constants.dart';
import 'package:popcrn_demo/widgets/popcrn_style_widgets/arrow_icon_widget.dart';

class ChooseButton extends StatelessWidget {
  final GestureTapCallback? onTap;
  final String text;

  const ChooseButton({Key? key, this.onTap, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black, width: 1),
        borderRadius: BorderRadius.circular(10),
      ),
      width: 270,
      height: 50,
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          color: Colors.transparent,
          child: Align(
            child: Padding(
              padding: const EdgeInsets.only(left: 60, right: 75),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    text,
                    style: GoogleFonts.poppins(
                      fontSize: 14.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.right,
                  ),

                  ///Constants space
                  const SizedBox(width: 16),
                  Flexible(
                    child: Transform.rotate(
                      angle: -90.0 * pi / 180,
                      child: Container(
                        alignment: const Alignment(-0.01, 0.1),
                        width: 18.0,
                        height: 18.0,
                        child: const ArrowIcon(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
