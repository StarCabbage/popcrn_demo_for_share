import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:popcrn_demo/constants/constants.dart';

class AvatarWidget extends StatelessWidget {
  final String imageAsset;

  const AvatarWidget({Key? key, required this.imageAsset}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ///Avatar image
        ClipOval(
          child: Image.asset(
            imageAsset,
            width: ConstantsThemeAvatar.imageSize,
            height: ConstantsThemeAvatar.imageSize,
          ),
        ),

        ///Avatar borders
        Container(
          width: ConstantsThemeAvatar.imageSize,
          height: ConstantsThemeAvatar.imageSize,
          decoration: BoxDecoration(
            border: Border.all(
                color: Colors.white, width: ConstantsThemeAvatar.borderWidth),
            borderRadius: ConstantsThemeAvatar.borderCircleGeometry,
          ),
        ),
      ],
    );
  }
}
