import 'package:flutter/material.dart';
import 'package:popcrn_demo/constants/constants.dart';

class ChipWidget extends StatelessWidget {
  final Color color;
  final double opacity;
  final String text;
  final void Function()? onTap;

  const ChipWidget({
    Key? key,
    this.color = ConstantsThemeChip.chipBasicBackground,
    this.opacity = ConstantsThemeChip.chipOpacity,
    required this.text,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _padding = ConstantsThemeChip.chipTextPadding;
    var _boxColor = color.withOpacity(opacity);
    var _decoration = BoxDecoration(
      borderRadius: ConstantsThemeChip.chipRadius,
      color: _boxColor,
    );
    Widget _widget = Padding(
      padding: _padding,
      child: Text(
        text,
        style: ConstantsThemeChip.chipTextStyle,
      ),
    );

    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: _decoration,
        child: _widget,
      ),
    );
  }
}
