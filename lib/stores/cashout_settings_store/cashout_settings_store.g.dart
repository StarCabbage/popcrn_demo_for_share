// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cashout_settings_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CashoutSettings on _CashoutSettings, Store {
  Computed<bool>? _$isEmptyComputed;

  @override
  bool get isEmpty => (_$isEmptyComputed ??=
          Computed<bool>(() => super.isEmpty, name: '_CashoutSettings.isEmpty'))
      .value;

  final _$countryAtom = Atom(name: '_CashoutSettings.country');

  @override
  String get country {
    _$countryAtom.reportRead();
    return super.country;
  }

  @override
  set country(String value) {
    _$countryAtom.reportWrite(value, super.country, () {
      super.country = value;
    });
  }

  final _$isFirstTimeAtom = Atom(name: '_CashoutSettings.isFirstTime');

  @override
  bool get isFirstTime {
    _$isFirstTimeAtom.reportRead();
    return super.isFirstTime;
  }

  @override
  set isFirstTime(bool value) {
    _$isFirstTimeAtom.reportWrite(value, super.isFirstTime, () {
      super.isFirstTime = value;
    });
  }

  final _$_CashoutSettingsActionController =
      ActionController(name: '_CashoutSettings');

  @override
  void clear() {
    final _$actionInfo = _$_CashoutSettingsActionController.startAction(
        name: '_CashoutSettings.clear');
    try {
      return super.clear();
    } finally {
      _$_CashoutSettingsActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setCountry(String country) {
    final _$actionInfo = _$_CashoutSettingsActionController.startAction(
        name: '_CashoutSettings.setCountry');
    try {
      return super.setCountry(country);
    } finally {
      _$_CashoutSettingsActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
country: ${country},
isFirstTime: ${isFirstTime},
isEmpty: ${isEmpty}
    ''';
  }
}
