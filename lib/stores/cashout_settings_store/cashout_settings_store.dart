import 'package:mobx/mobx.dart';

part 'cashout_settings_store.g.dart';

class CashoutSettings = _CashoutSettings with _$CashoutSettings;

abstract class _CashoutSettings with Store {
  static const selectDefault = 'Select country';
  @observable
  String country = selectDefault;

  @observable
  bool isFirstTime = true;

  @computed
  bool get isEmpty => country == selectDefault;

  @action
  void clear() {
    country = selectDefault;
  }

  @action
  void setCountry(String country) {
    isFirstTime = false;
    this.country = country;
  }
}
