// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cashout_settings_main_page_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CashoutSettingsMainPageStore on _CashoutSettingsMainPageStore, Store {
  final _$_CashoutSettingsMainPageStoreActionController =
      ActionController(name: '_CashoutSettingsMainPageStore');

  @override
  void exit2MainPage(BuildContext context) {
    final _$actionInfo = _$_CashoutSettingsMainPageStoreActionController
        .startAction(name: '_CashoutSettingsMainPageStore.exit2MainPage');
    try {
      return super.exit2MainPage(context);
    } finally {
      _$_CashoutSettingsMainPageStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void navigate2ChangeCountryPage(BuildContext context) {
    final _$actionInfo =
        _$_CashoutSettingsMainPageStoreActionController.startAction(
            name: '_CashoutSettingsMainPageStore.navigate2ChangeCountryPage');
    try {
      return super.navigate2ChangeCountryPage(context);
    } finally {
      _$_CashoutSettingsMainPageStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
