import 'package:mobx/mobx.dart';
import 'package:popcrn_demo/constants/constants.dart';
import 'package:popcrn_demo/navigation/navigator.dart';

part 'cashout_settings_main_page_store.g.dart';

class CashoutSettingsMainPageStore = _CashoutSettingsMainPageStore
    with _$CashoutSettingsMainPageStore;

abstract class _CashoutSettingsMainPageStore with Store {
  @action
  void exit2MainPage(BuildContext context) =>
      Navigator.pushReplacementNamed(context, NavigatorManager.mainPage);

  @action
  void navigate2ChangeCountryPage(BuildContext context) =>
      Navigator.pushReplacementNamed(
          context, NavigatorManager.cashoutSettingsPage);
}
