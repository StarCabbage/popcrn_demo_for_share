import 'package:mobx/mobx.dart';
import 'package:popcrn_demo/constants/constants.dart';
import 'package:popcrn_demo/navigation/navigator.dart';
import 'package:popcrn_demo/stores/cashout_settings_store/cashout_settings_store.dart';
import 'package:provider/provider.dart';

part 'cashout_settings_page_store.g.dart';

class CashoutSettingsPageStore = _CashoutSettingsPageStore
    with _$CashoutSettingsPageStore;

abstract class _CashoutSettingsPageStore with Store {
  @action
  void navigate2NextPage(BuildContext context) {
    final settings = Provider.of<CashoutSettings>(context, listen: false);
    if (!settings.isEmpty) {
      Navigator.pushReplacementNamed(context, NavigatorManager.webViewPage);
    }
  }
}
