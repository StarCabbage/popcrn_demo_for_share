import 'package:mobx/mobx.dart';
import 'package:popcrn_demo/constants/constants.dart';
import 'package:popcrn_demo/navigation/navigator.dart';
import 'package:popcrn_demo/stores/cashout_settings_store/cashout_settings_store.dart';
import 'package:provider/provider.dart';

part 'main_page_store.g.dart';

class MainPageStore = _MainPageStore with _$MainPageStore;

abstract class _MainPageStore with Store {
  @action
  void navigate2NextPage(BuildContext context) {
    final settings = Provider.of<CashoutSettings>(context, listen: false);
    if (settings.isFirstTime) {
      Navigator.pushReplacementNamed(
          context, NavigatorManager.cashoutSettingsPage);
    } else {
      Navigator.pushReplacementNamed(
          context, NavigatorManager.cashoutSettingsMainPage);
    }
  }
}
