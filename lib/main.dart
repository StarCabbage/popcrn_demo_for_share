import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:popcrn_demo/generated_resources/app_localizations.dart';
import 'package:popcrn_demo/root_widget.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  ///Localization setup
  AppLocalizations.supportedLocales.add(const Locale('en', 'US'));

  ///Locking orientation in portrait mode
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(const PopcrnDemoApp());
}
