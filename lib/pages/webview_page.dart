import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:popcrn_demo/navigation/navigator.dart';


class WebViewPage extends StatelessWidget {
  const WebViewPage({Key? key}) : super(key: key);

  toCashoutSettingsMainPage(BuildContext context) =>
      Navigator.pushReplacementNamed(
          context, NavigatorManager.cashoutSettingsMainPage);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Text(
            'WebView',
            style: TextStyle(color: Colors.black),
          ),
          leading: Container(),
        ),
        body: Center(
            child: TextButton(
          onPressed: () => toCashoutSettingsMainPage(context),
          child: const Text('Next page'),
        )));
  }
}
