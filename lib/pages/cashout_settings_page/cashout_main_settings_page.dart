import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:popcrn_demo/constants/constants.dart';
import 'package:popcrn_demo/generated_resources/gen_extensions/generated_str_extension.dart';
import 'package:popcrn_demo/stores/cashout_settings_main_page_store/cashout_settings_main_page_store.dart';
import 'package:popcrn_demo/stores/cashout_settings_store/cashout_settings_store.dart';
import 'package:popcrn_demo/widgets/popcrn_style_widgets/arrow_icon_widget.dart';
import 'package:popcrn_demo/widgets/popcrn_style_widgets/button.dart';
import 'package:provider/provider.dart';

import 'cashout_settings_page.dart';

class CashoutMainSettingsPage extends CashoutSettingsPage {
  const CashoutMainSettingsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final settings = Provider.of<CashoutSettings>(context);
    final cashoutSettingsMainPage =
        Provider.of<CashoutSettingsMainPageStore>(context);
    var cardPageStrings = strings.pages.cashoutSettingsPages.cardPage;
    return Material(
      color: Colors.white,
      child: SingleChildScrollView(
        child: SafeArea(
          minimum: const EdgeInsets.only(top: 63),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Column(
                  children: [
                    title(context),

                    ///Constants space
                    const SizedBox(height: 39.9),
                    cashoutImage,

                    ///Constants space
                    const SizedBox(height: 55),
                    SizedBox(
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: ConstantsTheme.padding),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              cardPageStrings.payoutsInfo.l(context),
                              style: ConstantsThemeCashoutSettingsPage
                                  .messageTextStyle,
                              textAlign: TextAlign.start,
                            ),

                            ///Constants space
                            const SizedBox(height: 36),
                            Text(
                              cardPageStrings.cardTemplateInfo.l(context),
                              style: ConstantsThemeCashoutSettingsPage
                                  .message2TextStyle,
                            ),

                            ///Constants space
                            const SizedBox(height: 10),
                            Text.rich(
                              TextSpan(
                                style: ConstantsThemeCashoutSettingsPage
                                    .message2TextStyle,
                                children: [
                                  TextSpan(
                                    text: cardPageStrings.country.l(context),
                                    style: ConstantsThemeCashoutSettingsPage
                                        .messageBoldTextStyle,
                                  ),
                                  TextSpan(
                                    text: settings.country,
                                  ),
                                ],
                              ),
                            ),

                            ///Constants space
                            const SizedBox(height: 52),
                            changeAccountCountry(context,
                                cardPageStrings.changeCountryButton.l(context)),
                          ],
                        ),
                      ),
                    ),

                    ///Constants space
                    const SizedBox(height: 71),
                    Button(
                      textButton:
                          cardPageStrings.manageAccountButton.l(context),
                    ),

                    ///Constants space
                    const SizedBox(height: 32),
                    GestureDetector(
                      onTap: () =>
                          cashoutSettingsMainPage.exit2MainPage(context),
                      child: Text(
                        cardPageStrings.saveExitButton.l(context),
                        style: GoogleFonts.poppins(
                          fontSize: 16.0,
                          color: ConstantsColors.buttonOrangeColor,
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),

                    ///Constants space
                    const SizedBox(height: 32)
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget changeAccountCountry(BuildContext context, String text) {
    final cashoutSettingsMainPage =
        Provider.of<CashoutSettingsMainPageStore>(context);
    return GestureDetector(
      onTap: () => cashoutSettingsMainPage.navigate2ChangeCountryPage(context),
      child: Container(
        color: Colors.transparent,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Text(
                text,
                style: ConstantsThemeCashoutSettingsPage.buttonTextStyle,
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 25.0),
              child: Transform.rotate(
                angle: 180.0 * pi / 180,
                child: const ArrowIcon(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
