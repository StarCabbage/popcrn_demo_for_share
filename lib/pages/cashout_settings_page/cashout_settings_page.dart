import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:popcrn_demo/constants/constants.dart';
import 'package:popcrn_demo/generated_resources/gen_extensions/generated_files_extension.dart';
import 'package:popcrn_demo/generated_resources/gen_extensions/generated_str_extension.dart';
import 'package:popcrn_demo/stores/cashout_settings_page_store/cashout_settings_page_store.dart';
import 'package:popcrn_demo/stores/cashout_settings_store/cashout_settings_store.dart';
import 'package:popcrn_demo/widgets/popcrn_style_widgets/button.dart';
import 'package:popcrn_demo/widgets/popcrn_style_widgets/choose_button.dart';
import 'package:provider/provider.dart';

class CashoutSettingsPage extends StatelessWidget {
  const CashoutSettingsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _selectCountryMessage = Container(
      constraints: const BoxConstraints(maxWidth: 336),
      child: Text(
        strings.pages.cashoutSettingsPages.selectCountryPage
            .plsSelectCountryMessage
            .l(context),
        style: ConstantsThemeCashoutSettingsPage.messageTextStyle,
        textAlign: TextAlign.center,
      ),
    );

    var _selectCountryButton = Observer(builder: (context) {
      final settings = Provider.of<CashoutSettings>(context);
      return ChooseButton(
          text: settings.country, onTap: () => showPicker(context));
    });
    var _connectButton = Observer(builder: (context) {
      final settings = Provider.of<CashoutSettings>(context);
      final cashoutSettingsPage =
          Provider.of<CashoutSettingsPageStore>(context);
      void onTap() => cashoutSettingsPage.navigate2NextPage(context);

      return Button(
        onTap: onTap,
        textButton: strings
            .pages.cashoutSettingsPages.selectCountryPage.connectButton
            .l(context),
        color: settings.isEmpty
            ? ConstantsColors.buttonGreyColor
            : ConstantsColors.buttonOrangeColor,
      );
    });
    return Material(
      color: Colors.white,
      child: SingleChildScrollView(
        child: SafeArea(
          minimum: const EdgeInsets.only(top: 63),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    title(context),

                    ///Constants space
                    const SizedBox(height: 39.9),
                    cashoutImage,

                    ///Constants space
                    const SizedBox(height: 55),
                    _selectCountryMessage,

                    ///Constants space
                    const SizedBox(height: 40 + 6),
                    _selectCountryButton,

                    ///Constants space
                    const SizedBox(height: 40),
                    _connectButton
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget get cashoutImage =>
      SvgPicture.asset(files.images.svgs.cashoutScreenImageSvg);

  Widget title(BuildContext context) => Text(
        strings.pages.cashoutSettingsPages.title.l(context),
        style: ConstantsThemeCashoutSettingsPage.appBarTitleTextStyle,
        textAlign: TextAlign.center,
      );

  void showPicker(BuildContext context) {
    var dialogStrings = strings
        .pages.cashoutSettingsPages.selectCountryPage.selectCountryDialog;
    var usaString = dialogStrings.actions.usaChooseAction.l(context);
    final settings = Provider.of<CashoutSettings>(context, listen: false);
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        title: Text(dialogStrings.title.l(context)),
        message: Text(dialogStrings.message.l(context)),
        actions: <CupertinoActionSheetAction>[
          CupertinoActionSheetAction(
            child: Text(usaString),
            onPressed: () {
              settings.setCountry(usaString);
              Navigator.pop(context);
            },
          ),
          CupertinoActionSheetAction(
            child: Text(dialogStrings.actions.clearAction.l(context)),
            onPressed: () {
              settings.clear();
              Navigator.pop(context);
            },
          ),
          CupertinoActionSheetAction(
            child: Text(dialogStrings.actions.cancelAction.l(context)),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
