import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:popcrn_demo/constants/constants.dart';
import 'package:popcrn_demo/generated_resources/gen_extensions/generated_files_extension.dart';
import 'package:popcrn_demo/generated_resources/gen_extensions/generated_str_extension.dart';
import 'package:popcrn_demo/pages/main_page/bottom_shadow_widget.dart';
import 'package:popcrn_demo/pages/main_page/top_shadow_widget.dart';
import 'package:popcrn_demo/stores/main_page_store/main_page_store.dart';
import 'package:popcrn_demo/widgets/popcrn_style_widgets/avatar_widget.dart';
import 'package:popcrn_demo/widgets/popcrn_style_widgets/chip_widget.dart';
import 'package:popcrn_demo/widgets/popcrn_style_widgets/simple_text_button.dart';
import 'package:provider/provider.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget _bShadow = const BottomShadowWidget();
    _bShadow = Positioned(bottom: 0, left: 0, right: 0, child: _bShadow);

    Widget _tShadow = const TopShadowWidget();
    _tShadow = Positioned(top: 0, left: 0, right: 0, child: _tShadow);

    Widget _userRow = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        _avatarWidget,

        ///Constants space
        const SizedBox(width: 14),
        Text(
          strings.pages.mainPage.content.person.l(context),
          style: ConstantsThemeMainPage.avatarRowTextStyle,
        ),
      ],
    );

    Widget _title = Text(
      strings.pages.mainPage.content.title.l(context),
      style: ConstantsThemeMainPage.titleTextStyle,
    );

    Widget page = Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _userRow,
            spaceWidget,

            ///Constants space
            const SizedBox(height: 3),

            _title,

            ///Constants space
            const SizedBox(height: 21),

            settingsChip(context)
          ],
        ),
        const Spacer(),
        Align(
          child: likeButton,
          alignment: Alignment.centerRight,
        ),
      ],
    );

    page = Positioned(
      bottom: ConstantsThemeMainPage.mainPageBottomPadding,
      left: ConstantsTheme.padding,
      right: ConstantsTheme.padding,
      child: page,
    );

    return Material(
      child: Stack(
        children: [
          cover,
          _tShadow,
          _bShadow,
          page,
          swipeUpButton(context),
        ],
      ),
    );
  }

  Widget settingsChip(BuildContext context) => ChipWidget(
        text: strings.pages.mainPage.chips.settingsChip.l(context),
        onTap: () {
          final mainPageStore =
              Provider.of<MainPageStore>(context, listen: false);
          mainPageStore.navigate2NextPage(context);
        },
      );

  Widget get likeButton => Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: SvgPicture.asset(files.images.svgs.likeButtonSvg),
      );

  Widget get cover => Positioned.fill(
      child: Image.asset(files.images.chocolateGirlPng, fit: BoxFit.cover));

  Widget swipeUpButton(BuildContext context) => Align(
        child: Padding(
          padding: const EdgeInsets.only(bottom: 30),
          child: SimpleTextButton(
            onPressed: () {},
            text: strings.pages.mainPage.buttons.swipeUpButton.l(context),
          ),
        ),
        alignment: Alignment.bottomCenter,
      );

  Widget get _avatarWidget =>
      AvatarWidget(imageAsset: files.images.templateProfileImagePng);

  Widget get spaceWidget => const SizedBox(height: 20);
}
