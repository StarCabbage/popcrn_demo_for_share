import 'package:flutter/material.dart';
import 'package:popcrn_demo/constants/constants.dart';

class TopShadowWidget extends StatelessWidget {
  const TopShadowWidget({Key? key}) : super(key: key);
  static const double shadowHeight = 230;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: shadowHeight,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: const Alignment(0.0, 0.95),
          colors: [
            ConstantsShadowColors.blackColorWithOpacity,
            ConstantsShadowColors.greyColor
          ],
        ),
      ),
    );
  }
}
