import 'package:flutter/material.dart';
import 'package:popcrn_demo/constants/constants.dart';

class BottomShadowWidget extends StatelessWidget {
  const BottomShadowWidget({Key? key}) : super(key: key);
  static const double shadowHeight = 413;
  static const Alignment alignmentContainer = Alignment(-0.21, 0.58);
  static const Alignment alignmentGradientBegin = Alignment(0.0, 0.8);
  static const Alignment alignmentGradientEnd = Alignment(0.0, -0.89);
  static const List<double> stops = [0.0, 0.778, 1.0];

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: alignmentContainer,
        height: shadowHeight,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: alignmentGradientBegin,
            end: alignmentGradientEnd,
            colors: [
              ConstantsShadowColors.black,
              ConstantsShadowColors.greyColor2,
              ConstantsShadowColors.greyColor
            ],
            stops: stops,
          ),
        ));
  }
}
