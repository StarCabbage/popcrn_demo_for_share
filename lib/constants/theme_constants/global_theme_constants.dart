import '../theme_constants.dart';

abstract class ConstantsTheme {
  static const double padding = 15.0;
  static const double bottomButtonPadding = padding * 2;
  static const double fontSizeTextButton = 12;

  static Color textColor = Colors.white;
  static TextTheme textTheme =
      GoogleFonts.poppinsTextTheme().apply(displayColor: textColor);
  static ThemeData primaryTheme = ThemeData.light()
    ..copyWith(textTheme: textTheme);
}
