import '../theme_constants.dart';

abstract class ConstantsThemeCashoutSettingsPage {
  static TextStyle appBarTitleTextStyle = GoogleFonts.poppins(
    fontSize: 20.0,
    height: 30 / 20,
    color: const Color(0xFF1C1C1C),
    fontWeight: FontWeight.w500,
  );

  static TextStyle messageTextStyle = GoogleFonts.poppins(
    fontSize: 16.0,
    color: Colors.black,
    height: 23 / 16,
  );

  static TextStyle message2TextStyle = GoogleFonts.poppins(
    fontSize: 16.0,
    color: Colors.black,
    height: 1.44,
  );
  static TextStyle messageBoldTextStyle = GoogleFonts.poppins(
    fontWeight: FontWeight.w500,
  );

  static TextStyle buttonTextStyle = GoogleFonts.poppins(
    fontSize: 16.0,
    color: Colors.black,
    fontWeight: FontWeight.w500,
  );
}
