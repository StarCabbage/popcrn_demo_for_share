import '../theme_constants.dart';

abstract class ConstantsThemeAvatar {
  static double imageSize = 32;
  static double borderWidth = 2;
  static BorderRadiusGeometry borderCircleGeometry = BorderRadius.circular(300);
}
