import '../theme_constants.dart';

abstract class ConstantsSimpleTextButton {
  static const double textLeading = 18;

  static const double textHeight =
      textLeading / ConstantsTheme.fontSizeTextButton;

  static TextStyle simpleTextButtonTextStyle(Color textColor) {
    return GoogleFonts.poppins().copyWith(
      color: textColor,
      height: textHeight,
      fontSize: ConstantsTheme.fontSizeTextButton,
    );
  }
}
