import '../theme_constants.dart';

abstract class ConstantsThemeButton {
  static const double buttonWidth = 220.0;
  static const double buttonHeight = 50;
  static BorderRadius buttonBorderRadius = BorderRadius.circular(10.0);

  static TextStyle buttonTextStyle = GoogleFonts.poppins(
    fontSize: 14.0,
    color: Colors.white,
    fontWeight: FontWeight.w500,
  );
}
