import '../theme_constants.dart';

abstract class ConstantsThemeMainPage {
  static double mainPageBottomPadding = 116;
  static double avatarRowFontSize = 16;
  static TextStyle avatarRowTextStyle = GoogleFonts.poppins(
      fontSize: avatarRowFontSize,
      fontWeight: FontWeight.w500,
      color: Colors.white);

  static TextStyle titleTextStyle = GoogleFonts.poppins(
    fontSize: 30.0,
    color: Colors.white,
    fontWeight: FontWeight.w600,
    height: 1.3,
  );
}
