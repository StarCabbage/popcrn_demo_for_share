import '../theme_constants.dart';

abstract class ConstantsThemeChip {
  static const double chipOpacity = .20;
  static const Color chipBasicBackground = Colors.white;
  static const EdgeInsets chipTextPadding =
      EdgeInsets.symmetric(horizontal: 34.5, vertical: 5.5);

  static BorderRadius chipRadius = BorderRadius.circular(18);
  static TextStyle chipTextStyle = GoogleFonts.poppins().copyWith(
    color: ConstantsTheme.textColor,
    fontWeight: FontWeight.w500,
    fontSize: ConstantsTheme.fontSizeTextButton,
  );
}
