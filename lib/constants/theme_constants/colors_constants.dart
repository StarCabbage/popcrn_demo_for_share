import '../theme_constants.dart';

abstract class ConstantsShadowColors {
  static Color greyColor = const Color(0xFF545454).withOpacity(0.0);
  static Color greyColor2 = Colors.grey[850]!.withOpacity(0.35);
  static Color blackColorWithOpacity = Colors.black.withOpacity(0.49);
  static Color black = Colors.black;
}

abstract class ConstantsColors {
  static const Color buttonOrangeColor = Color(0xFFFD8030);
  static const Color buttonGreyColor = Color(0xFF898989);
}
