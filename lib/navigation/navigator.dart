library navigator;

import 'package:flutter/material.dart';
import 'package:popcrn_demo/pages/cashout_settings_page/cashout_main_settings_page.dart';
import 'package:popcrn_demo/pages/cashout_settings_page/cashout_settings_page.dart';
import 'package:popcrn_demo/pages/main_page/main_page.dart';
import 'package:popcrn_demo/pages/webview_page.dart';

class NavigatorManager {
  static const String mainPage = '/';
  static const String cashoutSettingsPage = '/cashout_settings_page';
  static const String cashoutSettingsMainPage = '/cashout_settings_main_page';
  static const String webViewPage = '/webview_page';

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    if (settings.name == cashoutSettingsPage) {
      return MaterialPageRoute(
          builder: (_) => const CashoutSettingsPage(), settings: settings);
    }
    if (settings.name == cashoutSettingsMainPage) {
      return MaterialPageRoute(
          builder: (_) => const CashoutMainSettingsPage(), settings: settings);
    }

    if (settings.name == webViewPage) {
      return MaterialPageRoute(
          builder: (_) => const WebViewPage(), settings: settings);
    }

    /// default case
    return MaterialPageRoute(
        builder: (_) => const MainPage(), settings: settings);
  }

  static String get homePage => mainPage;
}
